��          �      �       H  2   I  	   |     �  /   �  -   �  ,   �      )      J     k  &   �  1   �  6   �  0     �  K  7   *  
   b     m  ;   �  @   �  9   	  )   C  ,   m  )   �  ?   �  ?     :   D  2                                                  	      
            A single company is associated to various schools. Companies Create company in Iomad Iomad: Could not assign course to company ID %d Iomad: Could not assign user to company ID %d Iomad: Could not assign user to course ID %d Iomad: Could not create company. Iomad: Could not delete company. Iomad: Could not edit company. Iomad: Could not get company category. Iomad: Could not unassign user from company ID %d Iomad: successfully unassigned user from company ID %d Please activate and configure the Moodle plugin. Project-Id-Version: Iomad plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:13+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Une même compagnie est associée à plusieurs écoles. Compagnies Créer la compagnie dans Iomad Iomad: Impossible d'assigner le cours à la compagnie ID %d Iomad: Impossible d'assigner l'utilisateur à la compagnie ID %d Iomad: Impossible d'assigner l'utilisateur au cours ID %d Iomad: Impossible de créer la compagnie. Iomad: Impossible de supprimer la compagnie. Iomad: Impossible d'éditer la compagnie. Iomad: La catégorie de la compagnie n'a pu être récupérée. Iomad: Impossible d'enlever l'utilisateur de la compagnie ID %d Iomad: l'utilisateur a été retiré de la compagnie ID %d Merci d'activer et de configurer le plugin Moodle. 