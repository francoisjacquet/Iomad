��          �      �       H  2   I  	   |     �  /   �  -   �  ,   �      )      J     k  &   �  1   �  6   �  0       K  6   \     �     �  8   �  :   �  3   -  &   a  )   �  '   �  6   �  3     @   E  .   �                                               	      
            A single company is associated to various schools. Companies Create company in Iomad Iomad: Could not assign course to company ID %d Iomad: Could not assign user to company ID %d Iomad: Could not assign user to course ID %d Iomad: Could not create company. Iomad: Could not delete company. Iomad: Could not edit company. Iomad: Could not get company category. Iomad: Could not unassign user from company ID %d Iomad: successfully unassigned user from company ID %d Please activate and configure the Moodle plugin. Project-Id-Version: Iomad plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:11+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-Bookmarks: -1,-1,3,-1,-1,-1,-1,-1,-1,-1
X-Poedit-SearchPath-0: .
 Una misma compañía está asociada a varias escuelas. Compañías Crear compañía en Iomad Iomad: No se pudo asignar el curso a la compañía ID %d Iomad: No se pudo asignar el usuario a la compañía ID %d Iomad: No se pudo asignar el usuario al curso ID %d Iomad: No se pudo crear la compañía. Iomad: No se pudo suprimir la compañía. Iomad: No se pudo editar la compañía. Iomad: no se pudo obtener la categoría de compañía. No se pudo quitar el usuario de la compañía ID %d Iomad: se quito la asignación del usuario a la compañía ID %d Por favor active y configure el plugin Moodle. 